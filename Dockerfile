FROM node:21-alpine3.18

COPY ./ /app/
WORKDIR /app

RUN cd /app
RUN npm install --include=optional sharp
RUN npm run build

EXPOSE 8000