
## Installation

0. create .env file as in example below
1. `npm i` / `npm ci`
2. `npm run build`
3. `npm start`

### .env example

```
# default: info
DEBUG_LEVEL=info

# REDIS SETTINGS
REDIS_HOST= # redis activates if this field exist (put localhost to turn on in local env)
REDIS_PORT=
REDIS_USERNAME=
REDIS_PASSWORD=

# DB SETTINGS
DB_HOST= # database activates if this field exist
DB_PORT=
DB_DATABASE=
DB_USER=
DB_PASSWORD=

#API SETTINGS
ORACLE_API_BASE_URL= # web3 activates if this field exist (web3 source fetches info from api)
ORACLE_APP_NAME=
ORACLE_APP_ID=
ORACLE_APP_KEY=
ORACLE_KEY_ACTIVE_TIME=

#S3 SETTINGS
S3_ENDPOINT= # filestorage activates if this field exist
S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=
S3_BUCKET_NAME=
S3_PUBLIC_ENDPOINT=
```

## Usage

### Fetch all information about token

```http://<ip>:8000/<chain_id>/<contract_address>/<token_id>```

Response:

```
{
    "tokenUrl": "https://api.envelop.is/metadata/11155111/0xc272fa876f00e0de04097285e4dad55acf9c3e35/0",
    "tokenJSON": {
        "tokenId": "0",
        "name": "ENVELOP ",
        "description": "ENVELOP Metadata API",
        "external_url": "https://app.envelop.is/token/11155111/0xc272fa876f00e0de04097285e4dad55acf9c3e35/0",
        "image": "https://envelop.is/assets/img/wnftdefault.gif"
    },
    "tokenImg": {
        "image": "https://pub-01611f727ccf4cebb8380fcc3c618229.r2.dev/image/0f354f0859f469701fe87ba54217c346",
        "contentType": "image/gif"
    }
}
```

### Fetch token url

```http://<ip>:8000/<chain_id>/<contract_address>/<token_id>/tokenurl```

Response:

```
{
    "token_uri": "https://api.envelop.is/metadata/11155111/0xc272fa876f00e0de04097285e4dad55acf9c3e35/0"
}
```

### Fetch token json

```http://<ip>:8000/<chain_id>/<contract_address>/<token_id>/json```

Response:

```
{
    "tokenId": "0",
    "name": "ENVELOP ",
    "description": "ENVELOP Metadata API",
    "external_url": "https://app.envelop.is/token/11155111/0xc272fa876f00e0de04097285e4dad55acf9c3e35/0",
    "image": "https://envelop.is/assets/img/wnftdefault.gif"
}
```

### Fetch token image

```http://<ip>:8000/<chain_id>/<contract_address>/<token_id>/image```

Response:

```
{
    "image": "https://pub-01611f727ccf4cebb8380fcc3c618229.r2.dev/image/dce53bcc087a7efd694911f7b5ae7a5f",
    "contentType": "image/png",
    "thumbnail": "https://pub-01611f727ccf4cebb8380fcc3c618229.r2.dev/thumbnail/dce53bcc087a7efd694911f7b5ae7a5f"
}
```

Response for other formats (svg, gif, mp4, etc):

```
{
    "image": "https://pub-01611f727ccf4cebb8380fcc3c618229.r2.dev/image/0f354f0859f469701fe87ba54217c346",
    "contentType": "image/gif"
}
```

### Fetch token image body

Returns Keep for backward compatibility for some services

```http://<ip>:8000/<chain_id>/<contract_address>/<token_id>/imagedata```

### Proxy url

```http://<ip>:8000/proxyurl?url=<url>```

## Local Dev Docker Env

## Current

Build
```bash
docker build -t fetcher-local .
```
Run
```bash
docker compose -f docker-compose-local.yaml up
```

## Old version
Build
```bash
docker run -it --rm  -v $PWD:/app node:21-alpine3.18 /bin/sh -c 'cd /app  && npm install --include=optional sharp && npm run build'
```

Build Docker
```bash
docker build -t fetcher-local .
```

Run redis (put)  -Optional
```bash
docker run -p 6379:6379 -it redis/redis-stack-server:latest
```

Run
```bash
docker compose -f docker-compose-local.yaml up
```

