
export type NFT = {
	chainId: number,
	contractAddress: string,
	tokenId: string,
}
export type TokenJSONType = {
	name?: string,
	image?: string,
	description?: string,
}
export type TokenImageType = {
	imageRaw: string,
	data?: ArrayBuffer,
	contentType?: string,
	thumbnail?: ArrayBuffer,
}
export type TokenImageLinkType = {
	imageRaw: string,
	image: string,
	contentType?: string,
	thumbnail?: string,
}