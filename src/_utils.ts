
import Web3, {
	Contract
} from 'web3';

import config from './app.config.json';
import { NFT } from './_types';

let _sessionCache: Array<{ key: string, data: any }> = [];
export const _setCacheItem = (key: string, data: any) => {
	_sessionCache = [
		..._sessionCache.filter((item: { key: string, data: any }) => { return item.key !== key }),
		{ key, data }
	]
}
export const _getCacheItem = (key: string): any | undefined => {
	const foundItem = _sessionCache.find((item: { key: string, data: any }) => { return item.key === key });
	return foundItem?.data || undefined;
}

export const combineURLs = (baseURL: string, relativeURL: string) => {
	return relativeURL
		? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
		: baseURL;
}

// ---------- PROCESS URLS ----------
export const escapeDataUrl = (url: string, token?: NFT): string => {
	if ( url.startsWith('data:') ) {
		return url
			.replaceAll('#', '%23')
			.replaceAll(' ', '%20')
			.replaceAll('?', '%3F')
			.replaceAll('@', '%40')
			.replaceAll('&', '%26')
			.replaceAll('$', '%24')
	}

	return url;
}
export const swarmUrl = (url: string, token?: NFT): string => {
	const swarmBaseUrls = config.SWARM_BASE_URLS;
	try {

		for (let idx = 0; idx < swarmBaseUrls.length; idx++) {
			const element = swarmBaseUrls[idx];
			if ( url.startsWith(element.prefix) ) {
				const tail = url.replaceAll(element.prefix, '');
				return combineURLs( element.baseUrl, tail );
			}
		}

		return url;
	} catch(ignored) { return url }
}
export const erc1155IdUrl = (url: string, token?: NFT) => {
	if ( token?.tokenId && url.includes('{id}') ) { return url.replaceAll('{id}', token.tokenId) }

	return url;
}
const URL_PROCESSORS = [
	swarmUrl,
	escapeDataUrl,
	erc1155IdUrl,
]

export const processUrl = (
	url: string,
	token?: NFT,
	processors?: Array<(url: string) => string>,
): string => {

	let output = url;
	const _processors = processors || URL_PROCESSORS;

	_processors.forEach((func) => { output = func(output, token) })

	return output;
}
// ---------- END PROCESS URLS ----------

export const getABI = async (chainId: number, contractAddress: string, typeName?: string) => {

	const abisCached: Array<{ name: string, abi: any }> = _getCacheItem('abis') || [];
	const abiFound = abisCached.find((item) => {
		return item.name.toLowerCase() === contractAddress.toLowerCase() || (
			typeName && item.name.toLowerCase() === typeName.toLowerCase()
		)
	});
	if ( abiFound ) { return abiFound.abi }

	const BASE_URL = process.env.ORACLE_API_BASE_URL;
	if ( BASE_URL ) {
		// try to get abi by chain/address
		const addressUrl = combineURLs(BASE_URL, `/abis/${chainId}/${contractAddress.toLowerCase()}.json`);
		try{
			const addressResp = await fetch(addressUrl);

			if ( addressResp.ok ) {
				try {
					const abiParsed = await addressResp.json();
					_setCacheItem('abis', [
						...(_getCacheItem('abis') || []).filter((item: { name: string, aby: any }) => { return item.name.toLowerCase() !== contractAddress.toLowerCase() }),
						{ name: contractAddress, abi: abiParsed }
					]);
					return abiParsed;
				} catch(ignored) {}
			}
		} catch(e: any) { console.log('Cannot fetch abi', contractAddress, typeName, e); }

		if ( typeName ) {
			// try to get abi by type name
			const typeUrl = combineURLs(BASE_URL, `/abis/${typeName.toLowerCase()}.json`);
			try{
				const typeResp = await fetch(typeUrl);

				if ( typeResp.ok ) {
					try {
						const abiParsed = await typeResp.json();
						_setCacheItem('abis', [
							...(_getCacheItem('abis') || []).filter((item: { name: string, aby: any }) => { return item.name.toLowerCase() !== typeName.toLowerCase() }),
							{ name: typeName, abi: abiParsed }
						]);
						return abiParsed;
					} catch(ignored) {}
				}
			} catch(e: any) { console.log('Cannot fetch abi', contractAddress, typeName, e); }
		}
	}

	if ( typeName ) {
		try {
			const localAbi = require(`./abis/${(typeName).toLowerCase() }.json`);
			_setCacheItem('abis', [
				...(_getCacheItem('abis') || []).filter((item: { name: string, aby: any }) => { return item.name.toLowerCase() !== typeName.toLowerCase() }),
				{ name: typeName, abi: localAbi }
			]);
			return localAbi;
		} catch(ignored) {}
	}

	throw new Error(`Cannot load ${chainId}/${contractAddress} abi`);
}
export const createContract = async (web3: Web3, contractType: string, contractAddress: string): Promise<Contract<any>> => {
	const chainId = await web3.eth.getChainId();
	const abi = await getABI(Number(chainId), contractAddress || '', contractType);
	const contract = new web3.eth.Contract(abi, contractAddress);
	return contract;
}