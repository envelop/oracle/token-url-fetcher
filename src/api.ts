
const crypto = require('node:crypto');

import {
	_getCacheItem,
	_setCacheItem,
	combineURLs
} from "./_utils";

export const createAuthToken = async () => {

	async function sha256(message: string) {
		const msgBuffer = new TextEncoder().encode(message);
		const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
		const hashArray = Array.from(new Uint8Array(hashBuffer));
		const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
		return hashHex;
	}
	async function signTimed(name: string, key: string) {
		const now = new Date().getTime();
		const timeBlock = parseInt(`${now / (parseInt(key_active || '0') * 1000)}`);

		return sha256(name+key+timeBlock)
	}

	const app_name   = process.env.ORACLE_APP_NAME;
	const app_id     = process.env.ORACLE_APP_ID;
	const app_key    = process.env.ORACLE_APP_KEY;
	const key_active = process.env.ORACLE_KEY_ACTIVE_TIME;

	if ( !app_name || !app_id || !app_key || !key_active ) {
		throw new Error('No app_id or app_key of key_active in .env');
	}
	const tempKey = await signTimed(app_name, app_key);

	return `${app_id}.${tempKey}`
}

export type ChainType = {
	chainId         : number,
	name            : string,
	colorCode       : string,
	RPCUrl          : string,
	symbol          : string,
	EIPPrefix       : string,
	decimals        : number,
	tokenIcon?      : string,
	networkIcon?    : string,
	isTestNetwork   : boolean,
	explorerBaseUrl : string,
	explorerName    : string,
	hasOracle       : boolean,
};
const parseChain = (resp: ChainType): ChainType => {

	_setCacheItem(`rpcUrl_${parseInt(`${resp.chainId}`)}`, resp.RPCUrl);

	return {
		chainId        : parseInt(`${resp.chainId}`),
		decimals       : parseInt(`${resp.decimals}`),
		name           : resp.name,
		symbol         : resp.symbol,
		colorCode      : resp.colorCode,
		EIPPrefix      : resp.EIPPrefix,
		explorerBaseUrl: resp.explorerBaseUrl,
		explorerName   : resp.explorerName,
		isTestNetwork  : `${resp.isTestNetwork}`.toLowerCase() === 'true',
		RPCUrl         : resp.RPCUrl,
		hasOracle      : `${resp.hasOracle}`.toLowerCase() === 'true',
	};
}
export const getChainParamsFromAPI = async (chainId: number): Promise<ChainType> => {

	const authToken = await createAuthToken();
	const BASE_URL = process.env.ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/chain_info/${chainId}`);

	let respParsed: ChainType | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});
		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		throw new Error('Cannot fetch chain info');
	}

	if ( !respParsed ) {
		throw new Error('Cannot fetch chain info');
	}

	return parseChain(respParsed);
}
export const getChainParamsAllFromAPI = async (): Promise<Array<ChainType>> => {

	const cached = _getCacheItem('availableChains');
	if ( cached ) { return cached; }

	const authToken = await createAuthToken();
	const BASE_URL = process.env.ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/chain_info/`);

	let respParsed: Array<ChainType> | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		throw new Error('Cannot fetch chain info');
	}

	if ( !respParsed ) {
		throw new Error('Cannot fetch chain info');
	}

	const output = respParsed.map((item) => { return parseChain(item) });
	_setCacheItem('availableChains', output);

	return output;
}

export type BlacklistItemType = {
	chainId: number,
	contractAddress: string | null,
	tokenId: string | null,
	baseUri: string | null,
};
export type _BlacklistItemType = {
	chain_id: number,
	contract_address: string | null,
	token_id: string | null,
	base_uri: string | null,
	user_address: string | null,
};
const decodeBlacklistItem = (item: _BlacklistItemType): BlacklistItemType => {
	return {
		chainId: item.chain_id,
		contractAddress: item.contract_address,
		tokenId: item.token_id,
		baseUri: item.base_uri,
	}
}
export const getTokenBlacklistOfChain = async (chainId: number): Promise<Array<BlacklistItemType>> => {

	const authToken = await createAuthToken();
	const BASE_URL = process.env.ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/tokens_blacklist/${chainId}`);

	let respParsed: Array<_BlacklistItemType> | undefined = undefined;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		throw new Error(`Cannot fetch tokens blacklist of ${chainId} chain`);
	}

	if ( !respParsed ) {
		throw new Error(`Cannot fetch tokens blacklist of ${chainId} chain`);
	}

	return respParsed.map((item) => { return decodeBlacklistItem(item); })
}
export const getTokenBlacklist = async (): Promise<Array<BlacklistItemType>> => {

	const chains = await getChainParamsAllFromAPI();
	let output: Array<BlacklistItemType> = [];

	for (let idx = 0; idx < chains.length; idx++) {
		const item = chains[idx];

		const list = await getTokenBlacklistOfChain(item.chainId);
		output = [
			...output,
			...list
		];

	}

	return output;
}