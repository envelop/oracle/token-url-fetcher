
import winston from 'winston';
import { loggerOptions } from '../logs.config';
const logger = winston.createLogger(loggerOptions);

import DataSource from '../datasources';

import {
	BlacklistItemType,
	getTokenBlacklist,
} from '../api';
import {
	TokenImageLinkType,
	TokenImageType,
	TokenJSONType
} from '../_types';

export default class BlacklistSource extends DataSource {

	prettyName = 'blacklist';

	private blacklist: Array<BlacklistItemType> | undefined = undefined;

	constructor() {
		super();
		logger.info(`Creating ${this.prettyName} source`);

		this.fetchBlacklist();
	}

	async fetchBlacklist() {
		try {
			this.blacklist = await getTokenBlacklist();
		} catch(e) {
			logger.error(`Cannot fetch blacklist: ${e}`);
		}
		if ( !this.blacklist || !this.blacklist.length ) {
			logger.warn(`Empty blacklist`);
			return;
		}
		logger.info(`Blacklist created for chains: ${[...new Set(this.blacklist.map((item) => { return item.chainId }))]}`);
	}

	isTokenBlocked(chainId: number, contractAddress: string, tokenId: string): boolean {
		if ( this.blacklist === undefined ) { return false; }
		if ( !this.blacklist.length ) { return false; }

		const foundRule = this.blacklist.find((item) => {
			if ( item.chainId !== chainId ) { return false; }

			if ( item.contractAddress === null ) { return false; }

			if ( item.tokenId === null ) {
				return item.contractAddress.toLowerCase() === contractAddress.toLowerCase();
			} else {
				return item.contractAddress.toLowerCase() === contractAddress.toLowerCase() &&
					`${item.tokenId}` === `${tokenId}`;
			}
		});
		if ( foundRule ) { return true; }

		return false;
	}
	async getTokenUrl(chainId: number, contractAddress: string, tokenId: string): Promise<string | null> {
		if ( this.isTokenBlocked(chainId, contractAddress, tokenId) ) {
			return 'service:blocked';
		}
		return null;
	}
	async getTokenJSON(chainId: number, contractAddress: string, tokenId: string): Promise<TokenJSONType | null> {
		if ( this.isTokenBlocked(chainId, contractAddress, tokenId) ) {
			return { image: 'service:blocked' };
		}
		return null;
	}
	async getTokenImage?(chainId: number, contractAddress: string, tokenId: string): Promise<TokenImageLinkType | null>{
		if ( this.isTokenBlocked(chainId, contractAddress, tokenId) ) {
			return {
				imageRaw: 'service:blocked',
				image: 'service:blocked',
			};
		}
		return null;
	}

	isUrlBlocked(url: string): boolean {
		if ( this.blacklist === undefined ) { return false; }
		if ( !this.blacklist.length ) { return false; }

		const foundRule = this.blacklist.find((item) => {
			if ( !item.baseUri ) { return false; }
			return url.toLowerCase().includes(item.baseUri.toLowerCase());
		});
		if ( foundRule ) { return true; }

		return false;
	}
	async getTokenJSONByUrl(url: string): Promise<TokenJSONType | null> {
		if ( this.isUrlBlocked(url) ) {
			return { image: 'service:blocked' };
		}
		return null;
	}
	async getTokenJSONForceByUrl(url: string): Promise<TokenJSONType | null> {
		if ( this.isUrlBlocked(url) ) {
			return { image: 'service:blocked' };
		}
		return null;
	}
	async getTokenImageByUrl(url: string): Promise<TokenImageLinkType | null> {
		if ( this.isUrlBlocked(url) ) {
			return {
				imageRaw: 'service:blocked',
				image: 'service:blocked',
			};
		}
		return null;
	}
	async getTokenImageForceByUrl(url: string): Promise<TokenImageLinkType | null> {
		if ( this.isUrlBlocked(url) ) {
			return {
				imageRaw: 'service:blocked',
				image: 'service:blocked',
			};
		}
		return null;
	}
	async getTokenImageDataByUrl(url: string): Promise<TokenImageType | null> {
		if ( this.isUrlBlocked(url) ) {
			return {
				imageRaw: 'service:blocked',
			};
		}
		return null;
	}
	async getTokenImageDataForceByUrl(url: string): Promise<TokenImageType | null> {
		if ( this.isUrlBlocked(url) ) {
			return {
				imageRaw: 'service:blocked',
			};
		}
		return null;
	}
}