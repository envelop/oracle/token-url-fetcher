
import winston from 'winston';
import { loggerOptions } from '../logs.config';
const logger = winston.createLogger(loggerOptions);

import { createClient, RedisClientType } from 'redis';

import DataSource from '.';

import {
	TokenImageLinkType,
	TokenJSONType
} from '../_types';

export default class RedisSource extends DataSource {

	prettyName = 'redis';

	private client: RedisClientType;

	constructor() {
		super();

		const REDIS_HOST     = process.env.REDIS_HOST;
		const REDIS_PORT     = process.env.REDIS_PORT;
		const REDIS_USERNAME = process.env.REDIS_USERNAME;
		const REDIS_PASSWORD = process.env.REDIS_PASSWORD;

		if ( process.env.REDIS_HOST     === undefined ) { throw new Error('No REDIS_HOST in .env');     }
		if ( process.env.REDIS_PORT     === undefined ) { throw new Error('No REDIS_PORT in .env');     }
		if ( process.env.REDIS_USERNAME === undefined ) { throw new Error('No REDIS_USERNAME in .env'); }
		if ( process.env.REDIS_PASSWORD === undefined ) { throw new Error('No REDIS_PASSWORD in .env'); }

		logger.info(`Creating ${this.prettyName} source`);
		this.client = createClient({
			url: `redis://${REDIS_USERNAME}:${REDIS_PASSWORD}@${REDIS_HOST}:${REDIS_PORT}`
		});
		this.client.on(
			'error',
			(err) => logger.error('Redis Client Error', err)
		);

		this.connectClient();
	}
	async connectClient() {
		await this.client.connect();
		logger.info('Connected to Redis');
	}

	async getTokenUrl(chainId: number, contractAddress: string, tokenId: string): Promise<string | null> {
		return await this.client.get(`tokenurl/${chainId}/${contractAddress}/${tokenId}`);
	}
	async updateTokenUrl(chainId: number, contractAddress: string, tokenId: string, tokenUrl: string): Promise<void> {
		await this.client.set(`tokenurl/${chainId}/${contractAddress}/${tokenId}`, tokenUrl);
	}

	async getTokenJSON(chainId: number, contractAddress: string, tokenId: string): Promise<TokenJSONType | null> {
		const obj = await this.client.get(`tokenjson/${chainId}/${contractAddress}/${tokenId}`);
		if ( obj == null ) { return null; }
		return JSON.parse(obj);
	}
	async updateTokenJSON(chainId: number, contractAddress: string, tokenId: string, tokenJson: TokenJSONType): Promise<void> {
		const objStringified = JSON.stringify(tokenJson)
		await this.client.set(`tokenjson/${chainId}/${contractAddress}/${tokenId}`, objStringified);
	}

	async getTokenImage(chainId: number, contractAddress: string, tokenId: string): Promise<TokenImageLinkType | null> {
		const img = await this.client.get(`tokenimg/${chainId}/${contractAddress}/${tokenId}`);
		if ( img === null ) { return null; }
		return JSON.parse(img);
	}
	async updateTokenImageLink(chainId: number, contractAddress: string, tokenId: string, tokenImg: TokenImageLinkType): Promise<void> {
		await this.client.set(`tokenimg/${chainId}/${contractAddress}/${tokenId}`, JSON.stringify(tokenImg));
	}

}