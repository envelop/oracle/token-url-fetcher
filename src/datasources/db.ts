
import winston from 'winston';
import { loggerOptions } from '../logs.config';
const logger = winston.createLogger(loggerOptions);

import DataSource from '.';

const pgp = require('pg-promise')()

export default class DBSource extends DataSource {

	prettyName = 'database';

	private db: any;

	constructor() {
		super();

		logger.info(`Creating ${this.prettyName} source`);
		this.createDB();
	}

	createDB() {

		const DB_HOST     = process.env.DB_HOST;
		const DB_PORT     = process.env.DB_PORT;
		const DB_USER     = process.env.DB_USER;
		const DB_PASSWORD = process.env.DB_PASSWORD;
		const DB_DATABASE = process.env.DB_DATABASE;

		if ( process.env.DB_HOST     === undefined ) { throw new Error('No DB_HOST in .env');     }
		if ( process.env.DB_PORT     === undefined ) { throw new Error('No DB_PORT in .env');     }
		if ( process.env.DB_USER     === undefined ) { throw new Error('No DB_USER in .env');     }
		if ( process.env.DB_PASSWORD === undefined ) { throw new Error('No DB_PASSWORD in .env'); }
		if ( process.env.DB_DATABASE === undefined ) { throw new Error('No DB_DATABASE in .env'); }

		logger.info(`Connecting to database postgres://${DB_USER}:***@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`);
		this.db = pgp(`postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`)
	}

	async getTokenUrl(chainId: number, contractAddress: string, tokenId: string): Promise<string | null> {
		let output = null;

		const query721 = `
			select t.token_uri
			from chain_${chainId}.token_721 t
			where t.contract_address = '${contractAddress}' and token_id = '${tokenId}'
		`;
		const query1155 = `
			select t.token_uri
			from chain_${chainId}.token_1155_balances t
			where t.contract_address = '${contractAddress}' and token_id = '${tokenId}'
		`
		try {
			const resp = await this.db.query(query721);
			output = resp[0].token_uri
		} catch(ignored) { }

		if ( output === undefined ) {
			try {
				const resp = await this.db.query(query1155);
				output = resp[0].token_uri
			} catch(ignored) { }
		}

		return output;
	}
	async updateTokenUrl(chainId: number, contractAddress: string, tokenId: string): Promise<void> {
		const query = `
			CALL public.request_notify(
				'nft_uri_update_handler',
				'${chainId};${contractAddress};${tokenId}'
			)
		`;

		try {
			await this.db.query(query);
		} catch(ignored) { }
	}

}