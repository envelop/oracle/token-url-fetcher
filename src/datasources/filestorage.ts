
import winston from 'winston';
import { loggerOptions } from '../logs.config';
const logger = winston.createLogger(loggerOptions);

const crypto = require('node:crypto');

import config from '../app.config.json';

import {
	GetObjectCommand,
	PutObjectCommand,
	S3Client
} from '@aws-sdk/client-s3';
const { FetchHttpHandler } = require("@smithy/fetch-http-handler");

import DataSource from '.';
import {
	TokenImageLinkType,
	TokenImageType
} from '../_types';

import sharp from 'sharp';
sharp.simd(false);
const THUMBNAIL_FORMATS = [
	'image/png',
	'image/jpg',
	'image/jpeg',
].map((item) => { return item.toLowerCase(); })

export default class FileStorage extends DataSource {

	prettyName = 'filestorage';

	private s3: S3Client | undefined;

	private S3_PUBLIC_ENDPOINT: string | undefined;
	private S3_ENDPOINT: string | undefined;
	private S3_ACCESS_KEY_ID: string | undefined;
	private S3_SECRET_ACCESS_KEY: string | undefined;
	private S3_BUCKET_NAME: string | undefined;

	constructor() {
		super();

		logger.info(`Creating ${this.prettyName} source`);

		this.createS3();
	}

	createS3() {

		this.S3_PUBLIC_ENDPOINT   = process.env.S3_PUBLIC_ENDPOINT;
		this.S3_ENDPOINT          = process.env.S3_ENDPOINT;
		this.S3_ACCESS_KEY_ID     = process.env.S3_ACCESS_KEY_ID;
		this.S3_SECRET_ACCESS_KEY = process.env.S3_SECRET_ACCESS_KEY;
		this.S3_BUCKET_NAME       = process.env.S3_BUCKET_NAME;

		if ( this.S3_PUBLIC_ENDPOINT   === undefined ) { throw new Error('No S3_PUBLIC_ENDPOINT in .env');   }
		if ( this.S3_ENDPOINT          === undefined ) { throw new Error('No S3_ENDPOINT in .env');          }
		if ( this.S3_ACCESS_KEY_ID     === undefined ) { throw new Error('No S3_ACCESS_KEY_ID in .env');     }
		if ( this.S3_SECRET_ACCESS_KEY === undefined ) { throw new Error('No S3_SECRET_ACCESS_KEY in .env'); }
		if ( this.S3_BUCKET_NAME       === undefined ) { throw new Error('No S3_BUCKET_NAME in .env');       }

		this.s3 = new S3Client({
			region: "auto",
			endpoint: this.S3_ENDPOINT,
			credentials: {
				accessKeyId: this.S3_ACCESS_KEY_ID,
				secretAccessKey: this.S3_SECRET_ACCESS_KEY,
			},
			requestHandler: new FetchHttpHandler({
				requestTimeout: 3*1000,
			}),
		});

	}

	private async encodeName(chainId: number, contractAddress: string, tokenId: string) {
		return crypto.createHash('md5').update(`${chainId}/${contractAddress}/${tokenId}`).digest('hex');;
	}

	async getTokenImage?(chainId: number, contractAddress: string, tokenId: string): Promise<TokenImageLinkType | null> {
		if ( !this.s3 ) { throw new Error('S3 is not connected') }
		const encodedFilename = await this.encodeName(chainId, contractAddress, tokenId);

		let output = null;

		// fetching image
		try {
			const res = await this.s3.send(new GetObjectCommand({
				Bucket: this.S3_BUCKET_NAME,
				Key: `image/${encodedFilename}`,
			}));
			output = {
				image: `${this.S3_PUBLIC_ENDPOINT}/image/${encodedFilename}`,
				contentType: res.ContentType,
				imageRaw: res.Metadata ? res.Metadata['x-original-image-url'] : ''
			}
			logger.debug(`Output with image: ${JSON.stringify(output)}`);
		} catch (err: any) {
			if ( err.Code.toLowerCase() === 'nosuchkey' ) {
				logger.warn(`No image for ${chainId}/${contractAddress}/${tokenId}`);
				return null;
			}
			throw err;
		}

		// fetching thumbnail
		try {
			await this.s3.send(new GetObjectCommand({
				Bucket: this.S3_BUCKET_NAME,
				Key: `thumbnail/${encodedFilename}`,
			}));
			output = {
				...output,
				thumbnail: `${this.S3_PUBLIC_ENDPOINT}/thumbnail/${encodedFilename}`,
			}
			logger.debug(`Output with thumbnail: ${JSON.stringify(output)}`);
		} catch (err: any) {
			logger.warn(`No thumbnail for ${chainId}/${contractAddress}/${tokenId}`);
		}

		return output;
	}
	async updateTokenImage?(chainId: number, contractAddress: string, tokenId: string, tokenImage: TokenImageType): Promise<TokenImageLinkType | null> {

		if ( tokenImage.imageRaw.toLowerCase() === 'service:blocked' ) {
			return {
				imageRaw: 'service:blocked',
				image: 'service:blocked',
			};
		}
		if ( !tokenImage.data || !tokenImage.contentType ) { return null; }
		if ( tokenImage.imageRaw.toLowerCase().startsWith('data:') ) {
			return {
				imageRaw: tokenImage.imageRaw,
				image: tokenImage.imageRaw,
				contentType: tokenImage.contentType,
			};
		}

		if ( !this.s3 ) { throw new Error('S3 is not connected') }
		const encodedFilename = await this.encodeName(chainId, contractAddress, tokenId);

		let output = null;

		try {
			await this.s3.send(new PutObjectCommand({
				Bucket: this.S3_BUCKET_NAME,
				Key: `image/${encodedFilename}`,
				Body: (tokenImage.data as any),
				ContentType: tokenImage.contentType,
				Metadata: { 'x-original-image-url': tokenImage.imageRaw }
			}));
			output = {
				imageRaw: tokenImage.imageRaw,
				image: `${this.S3_PUBLIC_ENDPOINT}/image/${encodedFilename}`,
				contentType: tokenImage.contentType,
			}
		} catch (err) {
			logger.error(`Cannot upload image ${chainId}/${contractAddress}/${tokenId}: ${err}`);
			throw err;
		}

		if ( THUMBNAIL_FORMATS.includes(tokenImage.contentType.toLowerCase()) ) {
			try {
				const resizeWidth = config.thumbnailSizeWidth || 200;
				const s = sharp(tokenImage.data)
					.resize({ width: resizeWidth });

				await this.s3.send(new PutObjectCommand({
					Bucket: this.S3_BUCKET_NAME,
					Key: `thumbnail/${encodedFilename}`,
					Body: (await s.toBuffer() as any),
					ContentType: tokenImage.contentType,
				}));
				output = {
					...output,
					thumbnail: `${this.S3_PUBLIC_ENDPOINT}/thumbnail/${encodedFilename}`
				}
			} catch (err) {
				logger.error(`Cannot create or upload thumbnail ${chainId}/${contractAddress}/${tokenId}: ${err}`);
				throw err;
			}
		}

		return output;
	}

}