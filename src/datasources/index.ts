
import {
	NFT,
	TokenImageLinkType,
	TokenImageType,
	TokenJSONType
} from "../_types";

export default abstract class DataSource {

	public abstract prettyName: string;

	getTokenUrl?(chainId: number, contractAddress: string, tokenId: string): Promise<string | null>;
	getTokenUrlForce?(chainId: number, contractAddress: string, tokenId: string): Promise<string | null>;
	updateTokenUrl?(chainId: number, contractAddress: string, tokenId: string, tokenUrl: string): Promise<void>;

	getTokenJSON?(chainId: number, contractAddress: string, tokenId: string): Promise<TokenJSONType | null>;
	getTokenJSONForce?(chainId: number, contractAddress: string, tokenId: string): Promise<TokenJSONType | null>;
	getTokenJSONByUrl?(url: string, token: NFT): Promise<TokenJSONType | null>;
	getTokenJSONForceByUrl?(url: string, token: NFT): Promise<TokenJSONType | null>;
	updateTokenJSON?(chainId: number, contractAddress: string, tokenId: string, tokenJSON: TokenJSONType): Promise<void>;

	getTokenImage?(chainId: number, contractAddress: string, tokenId: string): Promise<TokenImageLinkType | null>;
	getTokenImageForce?(chainId: number, contractAddress: string, tokenId: string): Promise<TokenImageLinkType | null>;
	getTokenImageByUrl?(url: string, token: NFT): Promise<TokenImageLinkType | null>;
	getTokenImageForceByUrl?(url: string, token: NFT): Promise<TokenImageLinkType | null>;
	getTokenImageDataByUrl?(url: string, token: NFT): Promise<TokenImageType | null>;
	getTokenImageDataForceByUrl?(url: string, token: NFT): Promise<TokenImageType | null>;
	updateTokenImage?(chainId: number, contractAddress: string, tokenId: string, tokenImage: TokenImageType): Promise<TokenImageLinkType | null>;
	updateTokenImageLink?(chainId: number, contractAddress: string, tokenId: string, tokenImage: TokenImageLinkType): Promise<void>;

}