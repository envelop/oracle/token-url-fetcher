
import winston from 'winston';
import { loggerOptions } from '../logs.config';
const logger = winston.createLogger(loggerOptions);

import DataSource from '../datasources';

import {
	NFT,
	TokenImageType,
	TokenJSONType
} from '../_types';
import { processUrl } from '../_utils';

export default class NetfetcherSource extends DataSource {

	prettyName = 'netfetcher';

	constructor() {
		super();
		logger.info(`Creating ${this.prettyName} source`);
	}

	async fetchJSON(tokenUrl: string, token: NFT): Promise<TokenJSONType | null> {

		if ( tokenUrl === 'service:blocked' ) { return { image: 'service:blocked' }; }
		const parsedUrl = processUrl(tokenUrl, token);
		logger.info(`Fetching json with url: ${tokenUrl}. Parsed: ${parsedUrl}`)
		try {
			const response = await fetch(parsedUrl, { signal: AbortSignal.timeout(5000) });
			if ( !response.ok || !response.body ) {
				logger.error(`Cannot fetch url: ${tokenUrl}`)
				return null;
			}

			const output = await response.json();

			return output;
		} catch(e) {
			logger.error(`Cannot fetch url: ${tokenUrl}, ${e}`)
		}

		return null;
	}
	async fetchImage(tokenImgUrl: string, token: NFT): Promise<TokenImageType | null> {

		if ( tokenImgUrl === 'service:blocked' ) { return { imageRaw: 'service:blocked' }; }

		const parsedUrl = processUrl(tokenImgUrl, token);
		logger.info(`Fetching json with url: ${tokenImgUrl}. Parsed: ${parsedUrl}`)
		try {
			const response = await fetch(parsedUrl, { signal: AbortSignal.timeout(5000) });
			if ( !response.ok || !response.body ) {
				logger.error(`Cannot fetch url: ${tokenImgUrl}`)
				return null;
			}

			return {
				imageRaw: tokenImgUrl,
				data: await response.arrayBuffer(),
				contentType: response.headers.get('content-type') || 'image/png',
			};
		} catch(e) {
			logger.error(`Cannot fetch url: ${tokenImgUrl}, ${e}`)
		}

		return null;
	}

	async getTokenJSONByUrl(url: string, token: NFT): Promise<TokenJSONType | null> {
		return await this.fetchJSON(url, token);
	}
	async getTokenJSONForceByUrl(url: string, token: NFT): Promise<TokenJSONType | null> {
		return await this.fetchJSON(url, token);
	}
	async getTokenImageDataByUrl(url: string, token: NFT): Promise<TokenImageType | null> {
		return await this.fetchImage(url, token);
	}
	async getTokenImageDataForceByUrl(url: string, token: NFT): Promise<TokenImageType | null> {
		return await this.fetchImage(url, token);
	}
}