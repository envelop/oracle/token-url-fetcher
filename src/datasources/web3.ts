
import winston from 'winston';
import { loggerOptions } from '../logs.config';
const logger = winston.createLogger(loggerOptions);

import DataSource from '.';

import Web3 from 'web3';
import {
	getChainParamsAllFromAPI,
	getChainParamsFromAPI
} from '../api';
import {
	createContract
} from '../_utils';

export default class Web3Source extends DataSource {

	prettyName = 'chain';

	web3Instances: Array<{ chainId: number, web3: Web3 }> = []; // { chainId: 11155111, web3: Web3() }

	constructor() {
		super();

		logger.info(`Creating ${this.prettyName} source`);
		this.initWeb3Instances();
	}

	async initWeb3Instances(): Promise<Array<{ chainId: number, web3: Web3 }>> {

		const chainInfos = await getChainParamsAllFromAPI();
		logger.info(`Initializing chains: ${chainInfos.map((item) => { return item.chainId }).join(',')}`);
		logger.debug(`Chain params: ${JSON.stringify(chainInfos)}`);

		chainInfos.forEach((item) => {
			const web3Instance = new Web3(item.RPCUrl);
			this.web3Instances = [
				...this.web3Instances.filter((iitem) => { return iitem.chainId !== item.chainId }),
				{
					chainId: item.chainId,
					web3: web3Instance,
				}
			];
		});

		return this.web3Instances;
	}

	async getWeb3Instance(chainId: number): Promise<Web3> {
		const foundCached = this.web3Instances.find((item) => { return item.chainId === chainId });
		if ( foundCached ) { return foundCached.web3 }

		const chainInfo = await getChainParamsFromAPI(chainId);
		const web3Instance = new Web3(chainInfo.RPCUrl);
		this.web3Instances = [
			...this.web3Instances.filter((item) => { return item.chainId !== chainId }),
			{
				chainId,
				web3: web3Instance,
			}
		];

		return web3Instance;
	}

	async getTokenUrl(chainId: number, contractAddress: string, tokenId: string): Promise<string | null> {
		const web3 = await this.getWeb3Instance(chainId);
		try {
			const contract721 = await createContract(web3, '_erc721', contractAddress);
			const output = await contract721.methods.tokenURI(tokenId).call() as any;
			logger.debug(`Output 721: ${JSON.stringify(output)}`);
			return output;
		} catch(err: any) {
			try {
				const contract1155 = await createContract(web3, '_erc1155', contractAddress);
				const output = await contract1155.methods.uri(tokenId).call() as any;
				logger.debug(`Output 1155: ${JSON.stringify(output)}`);
				return output;
			} catch(err: any) {
				throw err;
			}
		}
	}

	async getTokenUrlForce(chainId: number, contractAddress: string, tokenId: string): Promise<string | null> {
		return this.getTokenUrl(chainId, contractAddress, tokenId);
	}

}