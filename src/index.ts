
import express, {
	Request,
	Response,
	Application
} from 'express';
import { Readable } from "stream";
import 'dotenv/config';
import { processUrl } from './_utils';

import winston from 'winston';
import { loggerOptions } from './logs.config';
const logger = winston.createLogger(loggerOptions);

import {
	TokenImageLinkType,
	TokenJSONType
} from './_types';

import DataSource       from './datasources';
import BlacklistSource  from './datasources/blacklist';
import RedisSource      from './datasources/cache';
import DBSource         from './datasources/db';
import Web3Source       from './datasources/web3';
import FileStorage      from './datasources/filestorage';
import NetfetcherSource from './datasources/netfetcher';

const availableSources: Array<DataSource> = [];

const createSources = () => {

	if (
		process.env.ORACLE_API_BASE_URL !== undefined &&
		process.env.ORACLE_API_BASE_URL !== ''
	) { availableSources.push(new BlacklistSource()) }

	if (
		process.env.REDIS_HOST !== undefined &&
		process.env.REDIS_HOST !== ''
	) { availableSources.push(new RedisSource()) }

	if (
		process.env.DB_HOST !== undefined &&
		process.env.DB_HOST !== ''
	) { availableSources.push(new DBSource()) }

	availableSources.push(new NetfetcherSource());

	// web3 fetches chain params from api
	if (
		process.env.ORACLE_API_BASE_URL !== undefined &&
		process.env.ORACLE_API_BASE_URL !== ''
	) { availableSources.push(new Web3Source()) }

	if (
		process.env.S3_ENDPOINT !== undefined &&
		process.env.S3_ENDPOINT !== ''
	) { availableSources.push(new FileStorage()) }

}
createSources();

export const getTokenUrl = async (chainId: number, contractAddress: string, tokenId: string) => {

	let output: string | null = null;
	const updateQueue: Array<DataSource> = [];

	for (let idx = 0; idx < availableSources.length; idx++) {
		if ( output !== null ) { continue; }
		const src = availableSources[idx];

		if ( !src.getTokenUrl ) { continue; }

		try {
			logger.info(`Reading tokenurl from ${src.prettyName}`);
			output = await src.getTokenUrl(chainId, contractAddress, tokenId);
			logger.debug(`Response tokenurl from ${src.prettyName}: ${output}`);

			if ( output === null ) {
				if ( src.updateTokenUrl ) { updateQueue.push(src); }
			}
		} catch(e: any) {
			logger.error(`Cannot getTokenUrl from ${src.prettyName}: ${e}`);
		}
	}

	if ( output === null ) { return null; }

	for (let idx = 0; idx < updateQueue.length; idx++) {
		const src = updateQueue[idx];
		if ( !src.updateTokenUrl ) { continue; }

		logger.info(`Updating tokenurl in ${src.prettyName}`);
		src.updateTokenUrl(chainId, contractAddress, tokenId, output)
	}

	return output;
}
export const getTokenJSON = async (chainId: number, contractAddress: string, tokenId: string) => {

	let output: TokenJSONType | null = null;
	const updateQueue: Array<DataSource> = [];

	for (let idx = 0; idx < availableSources.length; idx++) {
		if ( output !== null ) { continue; }
		const src = availableSources[idx];

		if ( !src.getTokenJSON ) { continue; }

		try {
			logger.info(`Reading tokenJSON from ${src.prettyName}`);
			output = await src.getTokenJSON(chainId, contractAddress, tokenId);
			logger.debug(`Response tokenJSON from ${src.prettyName}: ${JSON.stringify(output)}`);

			if ( output === null ) {
				if ( src.updateTokenUrl ) { updateQueue.push(src); }
			}
		} catch(e: any) {
			logger.error(`Cannot getTokenJSON from ${src.prettyName}: ${e}`);
		}
	}

	if ( output === null ) {

		let tokenUrl = await getTokenUrl(chainId, contractAddress, tokenId);
		if ( tokenUrl === null ) { return null; }

		for (let idx = 0; idx < availableSources.length; idx++) {
			if ( output !== null ) { continue; }
			const src = availableSources[idx];

			if ( !src.getTokenJSONByUrl ) { continue; }
			try {
				logger.info(`Reading tokenJSON by url from ${src.prettyName}`);
				output = await src.getTokenJSONByUrl(tokenUrl, { chainId, contractAddress, tokenId });
				logger.debug(`Response tokenJSON from ${src.prettyName}: ${JSON.stringify(output)}`);
			} catch(e: any) {
				logger.error(`Cannot getTokenJSON from ${src.prettyName}: ${e}`);
			}
		}
		if ( output === null ) { return null; }
	}

	for (let idx = 0; idx < updateQueue.length; idx++) {
		const src = updateQueue[idx];
		if ( !src.updateTokenJSON ) { continue; }

		logger.info(`Updating tokenJSON in ${src.prettyName}`);
		src.updateTokenJSON(chainId, contractAddress, tokenId, output)
	}

	return output;
}
export const getTokenImg = async (chainId: number, contractAddress: string, tokenId: string) => {

	let output: TokenImageLinkType | null = null;
	const updateQueue: Array<DataSource> = [];

	for (let idx = 0; idx < availableSources.length; idx++) {
		if ( output !== null ) { continue; }
		const src = availableSources[idx];

		if ( !src.getTokenImage ) { continue; }

		try {
			logger.info(`Reading tokenImage from ${src.prettyName}`);
			output = await src.getTokenImage(chainId, contractAddress, tokenId);
			logger.debug(`Response tokenImage from ${src.prettyName}: ${JSON.stringify(output)}`);

			if ( output === null ) {
				if ( src.updateTokenUrl ) { updateQueue.push(src); }
			}
		} catch(e: any) {
			logger.error(`Cannot getTokenImg from ${src.prettyName}: ${e}`);
		}
	}

	if ( output === null ) {
		const tokenJSON = await getTokenJSON(chainId, contractAddress, tokenId);
		if ( tokenJSON === null || !tokenJSON.image ) { return null; }

		let image = null;
		for (let idx = 0; idx < availableSources.length; idx++) {
			if ( image !== null ) { continue; }
			const src = availableSources[idx];

			if ( !src.getTokenImageDataByUrl ) { continue; }
			try {
				logger.info(`Reading tokenImage by url from ${src.prettyName}`);
				image = await src.getTokenImageDataByUrl(tokenJSON.image, { chainId, contractAddress, tokenId });
				logger.debug(`Response tokenImage from ${src.prettyName}: ${JSON.stringify(image)}`);
			} catch(e: any) {
				logger.error(`Cannot getTokenJSON from ${src.prettyName}: ${e}`);
			}
		}
		if ( image === null ) { return null; }

		for (let idx = 0; idx < availableSources.length; idx++) {
			const src = availableSources[idx];
			if ( !src.updateTokenImage ) { continue; }

			logger.info(`Updating tokenImage in ${src.prettyName}`);
			output = await src.updateTokenImage(chainId, contractAddress, tokenId, image);
		}
	}

	if ( output === undefined || output === null ) { return null; }

	for (let idx = 0; idx < updateQueue.length; idx++) {
		const src = updateQueue[idx];
		if ( !src.updateTokenImageLink ) { continue; }

		logger.info(`Updating tokenImageLink in ${src.prettyName}`);
		src.updateTokenImageLink(chainId, contractAddress, tokenId, output)
	}

	return output;

}

export const updateTokenDataForce = async (chainId: number, contractAddress: string, tokenId: string) => {

	let output = null;

	// fetching token url via force-method
	let tokenUrl: string | null = null;
	for (let idx = 0; idx < availableSources.length; idx++) {
		if ( tokenUrl !== null ) { continue; }
		const src = availableSources[idx];

		if ( !src.getTokenUrlForce ) { continue; }

		logger.info(`Reading actual tokenurl from ${src.prettyName}`);
		tokenUrl = await src.getTokenUrlForce(chainId, contractAddress, tokenId);
		logger.debug(`Response tokenurl from ${src.prettyName}: ${tokenUrl}`);
	}
	if ( tokenUrl === null ) { return output; }
	output = {
		...output || {},
		tokenUrl
	}

	// updating tokenurl in all sources
	for (let idx = 0; idx < availableSources.length; idx++) {
		const src = availableSources[idx];
		if ( !src.updateTokenUrl ) { continue; }

		logger.info(`Updating tokenurl in ${src.prettyName}`);
		src.updateTokenUrl(chainId, contractAddress, tokenId, tokenUrl)
	}

	// fetch json by url
	let tokenJSON = null;
	for (let idx = 0; idx < availableSources.length; idx++) {
		if ( tokenJSON !== null ) { continue; }
		const src = availableSources[idx];

		if ( !src.getTokenJSONForceByUrl ) { continue; }
		try {
			logger.info(`Reading tokenJSON from ${src.prettyName}`);
			tokenJSON = await src.getTokenJSONForceByUrl(tokenUrl, { chainId, contractAddress, tokenId });
			logger.debug(`Response tokenJSON from ${src.prettyName}: ${JSON.stringify(tokenJSON)}`);
		} catch(e: any) {
			logger.error(`Cannot getTokenJSON from ${src.prettyName}: ${e}`);
		}
	}
	if ( tokenJSON === null ) { return output; }
	output = {
		...output || {},
		tokenJSON
	}

	// update json in all sources
	for (let idx = 0; idx < availableSources.length; idx++) {
		const src = availableSources[idx];
		if ( !src.updateTokenJSON ) { continue; }

		logger.info(`Updating tokenurl in ${src.prettyName}`);
		src.updateTokenJSON(chainId, contractAddress, tokenId, tokenJSON)
	}
	if ( !tokenJSON.image ) { return null; }

	// fetch image by url from json
	let tokenImg = null;
	for (let idx = 0; idx < availableSources.length; idx++) {
		if ( tokenImg !== null ) { continue; }
		const src = availableSources[idx];

		if ( !src.getTokenImageDataForceByUrl ) { continue; }
		try {
			logger.info(`Reading tokenJSON from ${src.prettyName}`);
			tokenImg = await src.getTokenImageDataForceByUrl(tokenJSON.image, { chainId, contractAddress, tokenId });
			logger.debug(`Response tokenJSON from ${src.prettyName}: ${JSON.stringify(tokenImg)}`);
		} catch(e: any) {
			logger.error(`Cannot getTokenJSON from ${src.prettyName}: ${e}`);
		}
	}
	if ( tokenImg === null ) { return null; }

	// update json on all sources
	let tokenImgLink = null;
	for (let idx = 0; idx < availableSources.length; idx++) {
		const src = availableSources[idx];
		if ( !src.updateTokenImage ) { continue; }

		logger.info(`Updating tokenurl in ${src.prettyName}`);
		tokenImgLink = await src.updateTokenImage(chainId, contractAddress, tokenId, tokenImg)
	}
	if ( tokenImgLink === null ) { return output; }
	output = {
		...output || {},
		tokenImg: tokenImgLink
	}

	for (let idx = 0; idx < availableSources.length; idx++) {
		const src = availableSources[idx];
		if ( !src.updateTokenImageLink ) { continue; }

		logger.info(`Updating tokenurllink in ${src.prettyName}`);
		src.updateTokenImageLink(chainId, contractAddress, tokenId, tokenImgLink)
	}

	return output;

}

// ---------- EXPRESS ----------
const app: Application = express();
const port = 8000;

app.listen(port, () => {
	logger.info(`Express started on port ${port}`);
});
// ---------- ROUTES ----------
app.get('/health', (_: Request, res: Response) => {
	res.send({ health: 'ok' });
});
app.get('/proxyurl/', async (req: Request, res: Response) => {

	const url = req.query.url;
	if ( !url ) { res.sendStatus(400); return; }

	const response = await fetch(processUrl(url.toString()));
	if ( !response.body ) {
		res.status(400);
		res.send(`Cannot fetch url: ${url}`)
	}
	res.status(response.status);
	res.appendHeader('content-type', response.headers.get('content-type') || '');

	Readable.fromWeb(response.body as any).pipe(res);
});


app.get('/:chainId/:contractAddress/:tokenId/update',
async (req: Request, res: Response) => {
	logger.info(`Updateing data for ${parseInt(req.params.chainId)}/${req.params.contractAddress}/${req.params.tokenId}. User-Agent: ${req.headers['user-agent']}`);
	let tokenUrl;
	try{
		tokenUrl = await updateTokenDataForce(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId)
	} catch ( ignored ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}

	if ( tokenUrl === undefined ) {
		res.status(404);
		res.send({ 'detail': 'Token not found' });
		return;
	}

	res.send({ token_uri: tokenUrl });
});
app.get('/:chainId/:contractAddress/:tokenId/tokenurl',
async (req: Request, res: Response) => {
	logger.info(`Fetching tokenurl for ${parseInt(req.params.chainId)}/${req.params.contractAddress}/${req.params.tokenId}. User-Agent: ${req.headers['user-agent']}`);
	let tokenUrl;
	try{
		tokenUrl = await getTokenUrl(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId);
	} catch ( err: any ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}

	if ( tokenUrl === undefined ) {
		res.status(404);
		res.send({ 'detail': 'Token not found' });
		return;
	}

	res.send({ token_uri: tokenUrl });
});
app.get('/:chainId/:contractAddress/:tokenId/json', async (req: Request, res: Response) => {
	logger.info(`Fetching json for ${parseInt(req.params.chainId)}/${req.params.contractAddress}/${req.params.tokenId}. User-Agent: ${req.headers['user-agent']}`);

	let tokenJSON;
	try{
		tokenJSON = await getTokenJSON(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId);
		if ( tokenJSON === null ) {
			res.status(400);
			res.send({ 'detail': 'Cannot fetch token info' });
			return;
		}
	} catch ( ignored ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}

	res.appendHeader('content-type', 'application/json');
	res.send(tokenJSON);
});
app.get('/:chainId/:contractAddress/:tokenId/image', async (req: Request, res: Response) => {
	logger.info(`Fetching image for ${parseInt(req.params.chainId)}/${req.params.contractAddress}/${req.params.tokenId}. User-Agent: ${req.headers['user-agent']}`);

	let tokenImg;
	try{
		tokenImg = await getTokenImg(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId);
		if ( tokenImg === null ) {
			res.status(400);
			res.send({ 'detail': 'Cannot fetch token info' });
			return;
		}
	} catch ( ignored ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}

	res.send(tokenImg);
});
// backward compatibility
app.get('/:chainId/:contractAddress/:tokenId/imagedata', async (req: Request, res: Response) => {
	logger.info(`Fetching imagedata for ${parseInt(req.params.chainId)}/${req.params.contractAddress}/${req.params.tokenId}. User-Agent: ${req.headers['user-agent']}`);

	const tokenImgUrl = await getTokenImg(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId);
	if ( !tokenImgUrl ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch image' });
		return;
	}

	const responseImg = await fetch(processUrl(tokenImgUrl.image));
	if ( !responseImg.body ) {
		res.status(400);
		res.send(`Cannot fetch url: ${tokenImgUrl.image}`)
	}
	res.status(responseImg.status);
	res.appendHeader('content-type', responseImg.headers.get('content-type') || '');

	Readable.fromWeb(responseImg.body as any).pipe(res);

});

app.get('/:chainId/:contractAddress/:tokenId', async (req: Request, res: Response) => {
	logger.info(`Fetching all info for ${parseInt(req.params.chainId)}/${req.params.contractAddress}/${req.params.tokenId}. User-Agent: ${req.headers['user-agent']}`);

	let output = {};

	let tokenUrlRaw;
	try{
		tokenUrlRaw = await getTokenUrl(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId)
	} catch ( ignored ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}
	output = {
		...output,
		tokenUrlRaw,
	};

	let tokenJSONBody;
	try{
		tokenJSONBody = await getTokenJSON(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId);
		if ( tokenJSONBody === null ) {
			res.status(400);
			res.send({ 'detail': 'Cannot fetch token info' });
			return;
		}
	} catch ( ignored ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}

	output = {
		...output,
		tokenJSONBody
	};

	let tokenImg;
	try{
		tokenImg = await getTokenImg(parseInt(req.params.chainId), req.params.contractAddress, req.params.tokenId);
		if ( tokenImg === null ) {
			res.status(400);
			res.send({ 'detail': 'Cannot fetch token info' });
			return;
		}
	} catch ( ignored ) {
		res.status(400);
		res.send({ 'detail': 'Cannot fetch token info' });
		return;
	}

	output = {
		...output,
		tokenImg
	};

	res.appendHeader('content-type', 'application/json');
	res.send(output);
});

app.get('/*', (_: Request, res: Response) => {
	res.send({ 'detail': 'Not found' });
});
// ---------- END ROUTES ----------
// ---------- END EXPRESS ----------