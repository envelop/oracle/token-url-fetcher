import winston, {
    format
} from "winston";

export const loggerOptions = {
	level: process.env.DEBUG_LEVEL || 'info',
	transports: [ new winston.transports.Console() ],
	format: format.combine(
		format.colorize(),
		format.simple()
	)
};